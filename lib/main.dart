import 'package:flutter/material.dart';
import 'package:circular_check_box/circular_check_box.dart';
import 'package:fid_widgets/fid_widgets.dart';

void main() => runApp(MaterialApp(
      home: MyApp(),
    ));

    class MyApp extends StatefulWidget {
  // name({Key key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
      bool isChecked = false;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.grey[250],
        appBar: AppBar(
          backgroundColor: Colors.grey[400],
          leading: Icon(Icons.cancel),
          title: Center(
            child: Text(
              "New Account",
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ),
         body: Padding(
          padding: const EdgeInsets.all(30.0),
          child: Column(
            children: <Widget>[
              TitledBoarderFormField(icon: Icons.email, title: "Email",),
              TitledBoarderFormField(icon: Icons.email, title: "Confirm Email",),
                                          TitledBoarderFormField(icon: Icons.lock, title: "Confirm Password",),
Row(
                children: <Widget>[
                     CircularCheckBox(
                    value: isChecked,
                    activeColor: Colors.yellow,
                    onChanged: (bool x) {
                    setState(() {});
                    },
                  ),
                  
                  Column( crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("I agree to the Terms of Service"),
                      Text("and Privacy Policy"),
                      Text(
                        "Terms of Service",
                        style: TextStyle(color: Colors.teal),
                      ),
                      Text(
                        "Privacy",
                        style: TextStyle(color: Colors.teal),
                      ),
                    ],
                  ),
                ],
              ),
              Row(
                children: <Widget>[
                  CircularCheckBox(
                    value: isChecked,
                    activeColor: Colors.yellow,
                    onChanged: (bool x) {
                    setState(() {
                    });
                    },
                  ),
                  Column( crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text("Keep me update about Qbit,"),
                      Text("news and promotions")
                    ],
                  ),
                ],
              ),
              SizedBox(
                height: 30.0,
              ),
              Expanded(
                child: Container(
                  color: Colors.yellow,
                  width: 7000.0,
                  height: 100.0,
                  child: IconButton(
                    icon: Icon(Icons.check),
                    color: Colors.white,
                    iconSize: 35.0,
                    onPressed: () {},
                  ),
                ),
              ),
            ],
          ),
        ),
        
      ),
    );



  }
  }
